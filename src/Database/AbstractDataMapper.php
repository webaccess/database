<?php

namespace Database;

abstract class AbstractDataMapper
{
    protected $adapter;
    protected $entityTable;
 
    public function __construct(\Database\DatabaseAdapterInterface $adapter) {
        $this->adapter = $adapter;
    }
  
    public function findOne(array $conditions = array())
    {
        $this->adapter->select($this->entityTable, $conditions);
 
        if (!$row = $this->adapter->fetch()) {
            return null;
        }
 
        return $this->createEntity($row);
    }
 
    public function findAll(array $conditions = array())
    {
        $entities = array();
        $this->adapter->select($this->entityTable, $conditions);
        $rows = $this->adapter->fetchAll();
 
        if ($rows) {
            foreach ($rows as $row) {
                $entities[] = $this->createEntity($row);
            }
        }
 
        return $entities;
    }
 
    abstract protected function createEntity(array $row);
}